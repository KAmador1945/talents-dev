from django.apps import AppConfig


class ProfessionalProfileConfig(AppConfig):
    name = 'professional_profile'

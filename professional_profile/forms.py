from django import forms
from django.db import models
from django.db.models import fields
from .models import ProfessionalProfileUsr

# Personal info
class PersonalInfo(forms.Form):
    # Creating fields
    ced = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'Cédula',
        })
    )
    address = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'Direccion actual',
        })
    )
    location = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'disabled':True,
        })
    )
    # Contact numbers
    tel = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'Télefono',
        })
    )
    mobile = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Móvil',
        })
    )
    other = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Otros',
        })
    )
    # Driver permision
    lic = forms.CharField(
        widget=forms.CheckboxInput(attrs={
            'type':'checkbox',
        })
    )
    cat = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'4A, 4B, 5A, etc.',
        })
    )
    # Using ProfessionalProfileUsr
    class Meta:
        models = ProfessionalProfileUsr
        fields = (
            'ced',
            'current_address',
            'tel_number',
            'mobile_number',
            'other_number',
            'lic_permission',
            'drive_lic_cat',
            'usr_token'
        )
        # create save method
        def save(self, commit=True):
            professional_profile = super().save(commit=False)
            professional_profile.usr_token = self.cleaned_data['usr_token']
            if commit:
                professional_profile.save()
            return professional_profile
# Socials networks
class SocialNetwork(forms.Form):
    # Creating fields
    linkedin = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'https://www.linkedin.com/'
        })
    )
    twitter = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'https://www.twitter.com/',
        })
    )
    facebook = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'https://www.facebook.com/'
        })
    )
    instagram = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'https://www.instagram.com/'
        })
    )
# studies 
class BasicStudies(forms.Form):
    # Creating fields
    highSchool_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Colegio'
        })
    )
    highSchool_start = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control',
        })
    )
    highSchool_end = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control'
        })
    )
    college_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Universidad'
        })
    )
    profession_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Profesion',
        })
    )
    college_start = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control',
        })
    )
    college_end = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control',
        })
    )

# Another studies masters, etc.
class MastersStudies(forms.Form):

    # creating fields
    ms_college_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Nombre Universidad',
        })
    )

    ms_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Nombre de Maestria'
        })
    )

    ms_start = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control',
        })
    )

    ms_end = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control'
        })
    )

class DoctorateStudies(forms.Form):

    # Creating fields
    dt_college_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'type':'text',
            'placeholder':'Nombre Universidad'
        })
    )

    dt_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'type':'text',
            'placeholder':'Nombre de Doctorado'
        })
    )

    dt_start = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control'
        })
    )

    dt_end = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control',
        })
    )

# jobs experiences
class JobsExperiences(forms.Form):
    
    # creating fields
    cp_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Nombre Empresa'
        })
    ) 

    cp_position = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Puesto Desempeñado'
        })
    )

    cp_start_in = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control',
        })
    )

    cp_end_in = forms.DateField(
        widget=forms.DateInput(attrs={
            'type':'date',
            'class':'form-control',
        })
    )

# Professional References
class ProfessionalRef(forms.Form):
    # Creating fields
    ref_name = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Nombres'
        })
    )
    ref_email = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'email',
            'class':'form-control',
            'placeholder':'Correo'
        })
    )
    ref_tel = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Télefono'
        })
    )
    other_ref = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Otro'
        })
    )
# Other skills and courses
class OtherSkills(forms.Form):
    # Creating fields
    language = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Ingles, Alemán ...'
        })
    )
    other = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Otros',
            'id':'id_courses_skills',
        })
    )
    course_1 = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Cursos',
            'id':'courses_skills',
        })
    )
    other_course = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Otros cursos'
        })
    )
# Personals references
class PersonalsRef(forms.Form):
    # Creating fields
    full_name_per_ref = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Nombres y Apellidos'
        })
    )
    mobile_per_ref = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Móvil',
        })
    )
    other_per_ref = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'text',
            'class':'form-control',
            'placeholder':'Otro'
        })
    )
    email_per_ref = forms.CharField(
        widget=forms.TextInput(attrs={
            'type':'email',
            'class':'form-control',
            'placeholder':'Correo'
        })
    )
# About me or bio
class AboutMe(forms.Form):
    # creating fields
    bio_me = forms.CharField(
        widget=forms.Textarea(attrs={
            'class':'form-control',
            'rows':5,
            'cols':20,
            'placeholder':'Sobre mi'
        })
    )
from django.contrib.auth.models import User
from django.db import models
from account.models import UserProfile

# Create your models here.
class ProfessionalProfileUsr(models.Model):
    
    # define field
    ced = models.CharField(max_length=100, null=False, blank=False)
    current_address = models.CharField(max_length=100, null=False, blank=False)
    tel_number = models.CharField(max_length=100, null=False, blank=False)
    mobile_number = models.CharField(max_length=100, null=False, blank=False)
    other_number = models.CharField(max_length=100, null=False, blank=False)

    # license
    lic_permission = models.BooleanField(blank=True, null=True)
    drive_lic_cat = models.CharField(max_length=100, null=True, blank=True)

    # create relationshipt between User model and Professional Profile
    fk_id_usr = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)    

    def __str__(self):
        return str(self.ced)

class SocialMediaLinksUsr(models.Model):
    # define field
    linkedin = models.CharField(max_length=100, null=False, blank=False)
    twitter = models.CharField(max_length=100, null=True, blank=True)
    facebook = models.CharField(max_length=100, null=True, blank=True)
    instgram = models.CharField(max_length=100, null=True, blank=True)

    # get id user
    fk_id_usr = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.linkedin)

class Studies(models.Model):
    # define field
    college_name = models.CharField(max_length=255, null=False, blank=False)
    profession_name = models.CharField(max_length=255, null=False, blank=False)
    start_in = models.DateTimeField(auto_now_add=False)
    end_in = models.DateTimeField(auto_now_add=False)

    # get id user
    fk_id_usr = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=False)

    def __str__(self):
        return str(self.college_name)

class ProfessionalExperiences(models.Model):
    # define field
    company_name = models.CharField(max_length=100, null=False, blank=False)
    position_name = models.CharField(max_length=100, null=False, blank=False)
    start_in = models.DateTimeField(auto_now_add=False)
    end_in = models.DateTimeField(auto_now_add=False)

    # get id user
    fk_id_usr = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=False)

    def __str__(self):
        return str(self.company_name)

class ProfessionalReferences(models.Model):
    # define field
    full_name = models.CharField(max_length=100, null=True, blank=False)
    mobile = models.CharField(max_length=100, null=True, blank=True)
    other = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)

    # get id user
    fk_id_usr = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.full_name)

class PersonalReferences(models.Model):
    # define field
    full_name = models.CharField(max_length=100, null=True, blank=False)
    mobile = models.CharField(max_length=100, null=True, blank=False)
    other = models.CharField(max_length=100, null=True, blank=False)
    email = models.EmailField(max_length=100, null=True, blank=False)

    # get id user
    fk_id_usr = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.full_name)

class Skills(models.Model):
    # define field
    language = models.CharField(max_length=100, null=True, blank=False)
    other_skill = models.CharField(max_length=100, null=True, blank=False)
    courses = models.CharField(max_length=100, null=True, blank=False)
    other_courses = models.CharField(max_length=100, null=True, blank=False)

    # get id user
    fk_id_usr = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.skill_name)
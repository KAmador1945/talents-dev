from django.urls import path
from professional_profile.views import CreateProfessionalProfile, ShowProfessionalInfo, CreateSocialMediaLink, CreateStudies, CreateProfessionalExperiences, CreateProfessionalRef, CreatePersonalRef, CreateSkills

app_name='professional'

# defining url patterns
urlpatterns = [
    # path('prof_info/<str:token>', ShowProfessionalInfo, name='professional_info'),
    path('professional_profile/edit/', CreateProfessionalProfile, name="create_prof"),
    path('professional_profile/edit/sm/', CreateSocialMediaLink, name="create_sm"),
    path('professional_profile/edit/std/', CreateStudies, name="studies"),
    path('professional_profile/edit/pe/',CreateProfessionalExperiences, name="professional_experiences"),
    path('professional_profile/edit/pr/',CreateProfessionalRef, name="professional_references"),
    path('professional_profile/edit/per_ref/',CreatePersonalRef,  name="personal_references"),
    path('professional_profile/edit/skills/',CreateSkills, name="Skills"),
    path('professional_profile/edit/<str:token>', ShowProfessionalInfo, name="professional_profile"),
]

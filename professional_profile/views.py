from django.shortcuts import render, redirect
from django.http import JsonResponse
from .forms import PersonalInfo, SocialNetwork, BasicStudies, JobsExperiences, OtherSkills, PersonalsRef, ProfessionalRef,AboutMe
from account.models import UserProfile, User
from professional_profile.models import ProfessionalProfileUsr, SocialMediaLinksUsr, Studies, ProfessionalExperiences, ProfessionalReferences, PersonalReferences, Skills

# Load professional info
def ShowProfessionalInfo(request, token):
    # Detect if user is connected
    if request.user.is_authenticated:
        # get id from UserProfile
        info_usr = UserProfile.objects.all().filter(token_usr=token)
        # data = ProfessionalProfileUsr.objects.all().filter(fk_id_usr=info_usr.user_id)
        # itera every object send from database
        for data_usr in info_usr:
            # make query
            main_info = ProfessionalProfileUsr.objects.all().filter(fk_id_usr=data_usr.user_id)
            social_media = SocialMediaLinksUsr.objects.all().filter(fk_id_usr=data_usr.user_id)
            main_study = Studies.objects.all().filter(fk_id_usr=data_usr.user_id)
            experiences = ProfessionalExperiences.objects.all().filter(fk_id_usr=data_usr.user_id)
            professional_ref = ProfessionalReferences.objects.all().filter(fk_id_usr=data_usr.user_id)
            personal_ref = PersonalReferences.objects.all().filter(fk_id_usr=data_usr.user_id)
            skills_user = Skills.objects.all().filter(fk_id_usr=data_usr.user_id)
            print("Mira esto ====>", skills_user)
            # detect if exist data
            if not main_info:
                # Load forms
                personal = PersonalInfo()
                social = SocialNetwork()
                studies = BasicStudies()
                jobs_exp = JobsExperiences()
                profe_ref = ProfessionalRef()
                per_ref = PersonalsRef()
                skills = OtherSkills()
                about_me = AboutMe()

                return render(request, 'ProfessionalProfiles/ProfessionalProfile.html', {
                    'info':info_usr,
                    'p_info':personal,
                    'sl':social,
                    'bs':studies,
                    'ej':jobs_exp,
                    'prof_r':profe_ref,
                    'pr':per_ref,
                    'sk':skills,
                    'am':about_me,
                })
            else:
                # Load forms
                social = SocialNetwork()
                studies = BasicStudies()
                jobs_exp = JobsExperiences()
                profe_ref = ProfessionalRef()
                per_ref = PersonalsRef()
                skills = OtherSkills()
                about_me = AboutMe()
                # run every object
                for p_info in main_info:
                    # load info in form
                    forms = PersonalInfo(
                        initial={
                            'ced':p_info.ced,
                            'address':p_info.current_address,
                            'tel':p_info.tel_number,
                            'mobile':p_info.mobile_number,
                            'other':p_info.other_number,
                            'lic':p_info.lic_permission,
                            'cat':p_info.drive_lic_cat,
                        }
                    )
                # Social Media
                for sm in social_media:
                    # load social media in form
                    form_sm = SocialNetwork(
                        initial={
                            'linkedin':sm.linkedin,
                            'twitter':sm.twitter,
                            'facebook':sm.facebook,
                            'instagram':sm.instgram,
                        }
                    )
                # Main study
                for main_std in main_study:
                    # load main study in form
                    form_main_std = BasicStudies(
                        initial={
                            'college_name':main_std.college_name,
                            'profession_name':main_std.profession_name,
                            'college_start':main_std.start_in,
                            'college_end':main_std.end_in,
                        }
                    )
                # Main expereriences
                for main_exp in experiences:
                    form_exp_job = JobsExperiences(
                        initial={
                            'cp_name':main_exp.company_name,
                            'cp_position':main_exp.position_name,
                            'cp_start_in':main_exp.start_in,
                            'cp_end_in':main_exp.end_in,
                        }
                    )
                
                # Main professional ref
                for ref_prof in professional_ref:
                    form_prof_ref = ProfessionalRef(
                        initial={
                            'ref_name':ref_prof.full_name,
                            'ref_tel':ref_prof.mobile,
                            'other_ref':ref_prof.other,
                            'ref_email':ref_prof.email,
                        }
                    )

                # Personal ref
                for pers_ref in personal_ref:
                    form_per_ref = PersonalsRef(
                        initial={
                            'full_name_per_ref':pers_ref.full_name,
                            'mobile_per_ref':pers_ref.mobile,
                            'other_per_ref':pers_ref.other,
                            'email_per_ref':pers_ref.email,
                        }
                    )
 
                return render(request, 'ProfessionalProfiles/ProfessionalProfile.html', {
                    'info':info_usr,
                    'p_info':forms,
                    'sl':form_sm,
                    'bs':form_main_std,
                    'ej':form_exp_job,
                    'prof_r':form_prof_ref,
                    'pr':form_per_ref,
                    'sk':skills,
                    'am':about_me,
                })     
    else:
        return redirect('account:sign_in')
# Create your views here.
def CreateProfessionalProfile(request):
    # detect if user is connected
    if request.user.is_authenticated:
        # detect method
        if request.method == 'POST':
            # get token from js
            usr_token = request.POST['token_usr']
            ced = request.POST['ced']
            location = request.POST['current_address']
            telep = request.POST['tel_number']
            mobile = request.POST['mobile_number']
            other = request.POST['other_number']
            licence = request.POST['lic_permission']
            drive_cat = request.POST['drive_lic_cat']
            # detect if exist data
            if not usr_token:
                # return nessage
                message = {
                    'error':'Error, no se encontro el token'
                }
                return JsonResponse(message)
            else:
                # detect if permission of drive is on or off
                if licence == "on":
                    # now get id of user
                    find_info = UserProfile.objects.get(token_usr=usr_token)
                    id_usr = User.objects.get(id=find_info.user_id)
                    print("Mira lo que encontre ========>", id_usr.id)
                    # check if exist data
                    if id_usr != "":
                        # insert info in table and create relationship
                        datas = ProfessionalProfileUsr(
                            ced=ced,
                            current_address=location,
                            tel_number=telep,
                            mobile_number=mobile,
                            other_number=other,
                            lic_permission=True,
                            drive_lic_cat=drive_cat,
                            fk_id_usr=id_usr,
                        )
                        # insert data
                        datas.save()
                        # detect if data isn't empty
                        if datas:
                            # create message
                            message = {
                                'message':'Token encontrado'
                            }
                            return JsonResponse(message)
                        else:
                            message = {
                                'error':'Operacion fallida'
                            }
                            return JsonResponse(message)
                    else:
                        message = {
                            'error':'No se encontro usuario'
                        }
                        return JsonResponse(message)
        else:
            message = {
                'error':'Oops, algo salio mal.'
            }
            return message
    else:
        return redirect('account:sign_in')

# create social media link
def CreateSocialMediaLink(request):
    # detect if user is connected
    if request.user.is_authenticated:
        # check method
        if request.method == 'POST':
            # get data from js
            usr_token = request.POST['token_usr']
            linkedin = request.POST['linkedin']
            twitter = request.POST['twitter']
            facebook = request.POST['facebook']
            instagram = request.POST['instagram']
            # get id user from relationship
            find_info = UserProfile.objects.get(token_usr=usr_token)
            id_usr = User.objects.get(id=find_info.user_id)
            # Load model 
            data = SocialMediaLinksUsr(
                linkedin=linkedin,
                twitter=twitter,
                facebook=facebook,
                instgram=instagram,
                fk_id_usr=id_usr,
            )
            # save info
            data.save()
            # detect if data isn't empty
            if data:
                message = {
                    'message':'Social media ready',
                }
                return JsonResponse(message)
        else:
            message = {
                'error':'Error, intentalo de nuevo'
            }
            return JsonResponse(message)
    else:
        return redirect('account:sign_in')
# Create studies
def CreateStudies(request):
    # check if user is connected
    if request.user.is_authenticated:
        # check method
        if request.method == 'POST':
            # get data from js
            usr_token = request.POST['token_usr']
            college = request.POST['college_name']
            profession_college = request.POST['profession_college']
            college_start = request.POST['college_start']
            college_end = request.POST['college_end']
            # get id user from relationship
            find_info = UserProfile.objects.get(token_usr=usr_token)
            id_usr = User.objects.get(id=find_info.user_id)
            # load model
            print(college, profession_college, college_start, college_end)
            data = Studies(
                college_name=college,
                profession_name=profession_college,
                start_in=college_start,
                end_in=college_end,
                fk_id_usr=id_usr,
            )
            # save info
            data.save()
            # detect if data isn't empty
            if data:
                message = {
                    'message':'Study has been added'
                }
                return JsonResponse(message)
            else:
                message = {
                    'error':'Oops, somethig result wrong'
                }
                return JsonResponse(message)
    else:
        return redirect('account:sign_in')
# create professional experiences
def CreateProfessionalExperiences(request):
    # check if user is connected
    if request.user.is_authenticated:
        # check method
        if request.method=='POST':
            # get data from js
            usr_token = request.POST['token_usr']
            company_name = request.POST['company_name']
            position_name = request.POST['position_name']
            start_in = request.POST['cp_start']
            end_in = request.POST['cp_end']
            # get id user from relationship
            find_info = UserProfile.objects.get(token_usr=usr_token)
            id_usr = User.objects.get(id=find_info.user_id)
            # load model
            data = ProfessionalExperiences(
                company_name=company_name,
                position_name=position_name,
                start_in=start_in,
                end_in=end_in,
                fk_id_usr=id_usr,
            )
            # save info
            data.save()
            # detect if data isn't empty
            if data:
                message={
                    'message':'Professional experiences has been added'
                }
                return JsonResponse(message)
            else:
                message={
                    'error':'Oops something result wrong'
                }
                return JsonResponse(message)
        else:
            message={
                'error':'Opps we dont use this method',
            }
            return JsonResponse(message)
    else:
        return redirect('account:sign_in')
            
# Create personal references
def CreateProfessionalRef(request):
    # check if user is connected
    if request.user.is_authenticated:
        # check method
        if request.method=='POST':
            # get data from js
            usr_token = request.POST['token_usr']
            full_name = request.POST['full_name_ref']
            mobile = request.POST['mobile']
            other = request.POST['other']
            email = request.POST['email']
            # debug data
            print("Mira esto ===>", email)
            # get id user from relationship
            find_info = UserProfile.objects.get(token_usr=usr_token)
            id_usr = User.objects.get(id=find_info.user_id)
            # load model
            data = ProfessionalReferences(
                full_name=full_name,
                mobile=mobile,
                other=other,
                email=email,
                fk_id_usr=id_usr,     
            )
            # safe data
            data.save()
            # detect if data isn't empty
            if data:
                message = {
                    'message':'Professional references has been added'
                }
                return JsonResponse(message)
            else:
                message={
                    'error':'Oops something result wrong'
                }
                return JsonResponse(message)
        else:
            message={
                'message':'Oops we dont use this method'
            }
            return JsonResponse(message)
    else:
        return redirect('account:sing_in')

# Create personal references
def CreatePersonalRef(request):
    # check if user is connected
    if request.user.is_authenticated:
        # check method
        if request.method == 'POST':
            # get data from js
            usr_token = request.POST['token_usr']
            full_name = request.POST['name_per_ref']
            per_ref_mobile = request.POST['mobile_per_ref']
            per_ref_other_mobile = request.POST['other_numb_per_ref']
            email_ref_per = request.POST['email_ref_per']
            # find info
            find_info=UserProfile.objects.get(token_usr=usr_token)
            id_usr=User.objects.get(id=find_info.user_id)
            # load model
            data = PersonalReferences(
                full_name=full_name,
                mobile=per_ref_mobile,
                other=per_ref_other_mobile,
                email=email_ref_per,
                fk_id_usr=id_usr,
            )
            # safe data
            data.save()
            # detect if data isn't empty
            if data:
                message = {
                    'message':'Personal references has been added',
                }
                return JsonResponse(message)
            else:
                message = {
                    'error':'Oops, something result wrong'
                }
                return JsonResponse(message)
        else:
            message = {
                'error':'Oops, something result wrong'
            }
            return JsonResponse(message)
    else:
        return redirect('account:sign_in')

# Create skills 
def CreateSkills(request):
    # check if user is connected
    if request.user.is_authenticated:
        # check method
        if request.method == 'POST':
            # get data from js
            usr_token = request.POST['usr_token']
            language = request.POST['language']
            other = request.POST['other_skills']
            courses = request.POST['courses_study']
            other_courses = request.POST['other_courses']
            # find info
            find_info = UserProfile.objects.get(token_usr=usr_token)
            id_usr = User.objects.get(id=find_info.user_id)
            # load model
            data = Skills(
                language=language,
                other_skill=other,
                courses=courses,
                other_courses=other_courses,
                fk_id_usr=id_usr,
            )
            # save data
            data.save()
            # detect if data isn't empty
            if data:
                message = {
                    'message':'Skills has been added'
                }
                return JsonResponse(message)
            else:
                message = {
                    'error':'Oops, something result wrong'
                }
                return JsonResponse(message)
        else:
            message = {
                'error':'Oops, something result wrong'
            }
            return JsonResponse(message)
    else:
        return redirect('account:sign_in')


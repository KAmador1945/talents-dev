#!/bin/bash

NAME="4-Talents"
DJANGODIR=/var/www/4-talents/
SOCKFILE=/var/www/4-talents/run/gunicorn.socket
USER=root
GROUP=root
NUM_WORKERS=3
DJANGO_SETTINGS_MODULE=talents.settings
DJANGO_WSGI_MODULE=talents.wsgi
echo "Deploying $NAME"

# Active virtual env using pipenv
cd $DJANGODIR
source /root/.local/share/virtualenvs/4-talents-ND3aYwme/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Creating directory if not exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Execute django app
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--bind=unix:$SOCKFILE \
--log-level=debug \
--log-file=-



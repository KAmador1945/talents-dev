from django.urls import path
from .views import SignIn, SignUp, SignOut, ManageAccount, GetAccountInfo, UpdateAccount, Update_Pic, UpdatePassword
from professional_profile.views import CreateProfessionalProfile


app_name = 'account'

urlpatterns = [
	path('sign_in/', SignIn, name='sign_in'),
	path('sign_up/', SignUp, name='sign_up'),
	path('signout', SignOut, name='sign_out'),
	path('manage/', ManageAccount, name='manage'),
	path('manage/edit/<str:token>', GetAccountInfo, name='edit_account'),
	path('manage/edit/ua/', UpdateAccount, name='update_account'),
	path('manage/edit/pp/<str:token>', Update_Pic, name='update_pic'),
	path('manage/edit/up/', UpdatePassword, name='update_pass'),
]

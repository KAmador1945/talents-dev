from django.db import models
from django.contrib.auth.models import User

# Creating Manager
class UserProfileManager(models.Manager):
	def get_queryset(self):
		return super().get_queryset().filter(active=True)

# Load all departaments or state of country
class CurrentLocationUsers(models.Model):

	# define fileds
	location_name = models.CharField(max_length=100, null=True, blank=True)

	# return object to string
	def __str__(self):
		return str(self.location_name)
	
# Create your models here.
class UserProfile(models.Model):

	# Managers
	objects = models.Manager()
	UserProfiled = UserProfileManager()

	# Loading User model
	user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
	
	# Creating new fields
	token_usr = models.CharField(max_length=120, null=True, blank=True, unique=True)
	p_profile = models.ImageField(upload_to='images/p_profile', null=True, blank=True)
	profession = models.CharField(max_length=100, null=True, blank=True)
	departamento = models.ForeignKey(CurrentLocationUsers, null=True, on_delete=models.CASCADE)
	status = models.CharField(max_length=20, default="INACTIVE", editable=True)
	active = models.BooleanField(default=False)
	
	
	# returning string object
	def __str__(self):
		return str(self.user.username)

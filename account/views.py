from django.contrib.sites import requests
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.decorators import login_required
from .forms import SignInForm, SignUpFormExtended, UpdateAccountForm
from .models import UserProfile, CurrentLocationUsers
from SendMail.views import EmailThreat

from django.contrib import messages
from django.http import JsonResponse
from django.core.mail import send_mail

# Setting
from django.contrib.sites.shortcuts import get_current_site
import secrets

# New settings to send email
from django.core.mail import EmailMultiAlternatives

from django.template import Context
from django.template.loader import render_to_string
import json
import urllib
from django.conf import settings

# Sign In
def SignIn(request):

    # check if user is active
    if request.user.is_authenticated:
        return redirect('/Home/')
    # check method
    if request.method == "POST":

        username = request.POST['username']
        password = request.POST['password']

        # detect if exist any data
        if username is not None and password is not None:

            # authenticate method
            user = authenticate(
                username=username,
                password=password
            )

            # detect if find any info
            if user is not None:
                # qs
                usr = User.objects.get(username=username)
                """
					SELECT * FROM USERS WHERE USERNAME ='USER';
				"""

                print("Encontre esto ==>", usr)

                # gets info to send email
                email_usr = usr.email

                # Info
                os = request.META['HTTP_USER_AGENT'].split()[1:-7]
                ip = request.META['REMOTE_ADDR']
                browser = request.META['HTTP_USER_AGENT'].split()[12]
                url = f"https://freegeoip.app/json/{ip}"
                # getting info from server
                datas = urllib.request.urlopen(url)
                str_datas = datas.read().decode('utf-8')
                obj = json.loads(str_datas)
                print("Mira esto hombre ==>", obj["country_name"])

                # get domain
                domain = get_current_site(request).domain

                # define context
                Context = {
                    'username': user,
                    'os': os,
                    'client': browser,
                    'domain': domain,
                    'origin': ip,
                    'country': obj["country_name"],
                    "time_zone": obj["time_zone"],
                }

                # # email parameter
                # subject = "4-Talents - Alerta de Seguridad"
                # text_body = render_to_string(
                #     'Account/Email/message_body.txt', Context)
                # html_body = render_to_string(
                #     'Account/Email/message_login.html', Context)
                # from_mail = "riosk241@gmail.com"
                # to = [email_usr]
                # mail = EmailMultiAlternatives(
                #     subject=subject, from_email=from_mail, to=to, body=text_body)
                # mail.attach_alternative(html_body, "text/html")
                # mail.send()

                # # verify if email has been send and start session
                # if mail:
                #     # create session
                #     login(request, user)

                #     # message
                #     data = {
                #         "message": "Welcome to 4-Talents",
                #     }

                #     return JsonResponse(data)

                login(request, user)
                data = {
                    "message":"Welcome to 4-Talents"
                }
                return JsonResponse(data)
            else:
                # create message
                data = {
                    "error": "Comprueba tus credenciales"
                }
                # send message
                return JsonResponse(data)

    form = SignInForm()
    return render(request, 'SignIn/SignIn.html', {
        'forms': form,
    })

# Sign Up
def SignUp(request):

    # Check if user is active
    if request.user.is_authenticated:
        return redirect('/Home/')
    # check method
    if request.method == "POST":

        # Access to form
        form = SignUpFormExtended(request.POST)

        # Check if form is valid
        if form.is_valid():

            # get data from form
            username = form.cleaned_data.get('username')
            email = form.cleaned_data.get('email')
            password1 = form.cleaned_data.get('password1')
            password2 = form.cleaned_data.get('password2')

            # Compare passwords
            if password1 == password2:
                # Check if User exist
                if User.objects.filter(username=username).exists():
                    # message
                    data = {
                        'message': 'El usuario ya existe',
                    }

                    return JsonResponse(data)

                elif User.objects.filter(email=email):
                    # message
                    data = {
                        'message': 'El correo ya existe'
                    }

                    return JsonResponse(data)
                else:
                    # Create user
                    user = form.save(commit=False)
                    user.save()

                    # authenticate method
                    user = authenticate(
                        username=username,
                        password=password1,
                    )

                    # validate session
                    if user is not None:
                        print("Encontre esto ===>", user)

                        # qs
                        qs = User.objects.get(username=username)

                        # generate token
                        token = secrets.token_hex(16)

                        # get id
                        id_usr = qs.id

                        # create status account and token
                        new_data = UserProfile.objects.create(
                            user_id=id_usr,
                            token_usr=token,
                        )

                        # check if status has been create
                        if new_data:
                            print("Status has been created")

                            # Path to verification view
                            domain = get_current_site(request).domain
                            print("Encontre esto ====>", domain)

                            # get token
                            usr_key = new_data.token_usr

                            print("TOKEN ===>", usr_key)

                            # Context = {
                            #     'token': usr_key,
                            #     'domain': domain
                            # }

                            # get email
                            # subject = "4-Talents - Activar Cuenta"
                            # # plaintext_context = Context(autoescape=True)
                            # text_body = render_to_string(
                            #     'Account/Email/message_body.txt', Context)
                            # html_body = render_to_string(
                            #     'Account/Email/message_body.html', Context)
                            # from_mail = "riosk241@gmail.com"
                            # to = [email]

                            # prepare mail
                            # mail = EmailThreat(subject, message, from_mail, to)
                            # mail = EmailMultiAlternatives(
                            #     subject=subject, from_email=from_mail, to=to, body=text_body)
                            # mail.attach_alternative(html_body, "text/html")
                            # mail.send()

                            # check if email has been send
                            # if mail:
                            #     # Create session
                            #     login(request, user)

                                # # message
                                # data = {
                                #     'message': f"Bienvenido {username} a 4-Talents",
                                #     'usr': f"{usr_key}",
                                # }

                            #     # returning message
                            #     return JsonResponse(data)
                            # else:
                            #     # message
                            #     data = {
                            #         'message': 'Oops a ocurrido un error, intenta de nuevo'
                            #     }

                            login(request, user)

                            # message
                            data = {
                                'message': f"Bienvenido {username} a 4-Talents",
                                'usr': f"{usr_key}",
                            }

                            return JsonResponse(data)
                        else:

                            data = {
                                'message': 'El estado de la cuenta no ha sido creado',
                            }

                            return JsonResponse(data)
                    else:
                        data = {
                            'message': 'Comprueba tu información e intentalo de nuevo',
                        }

                        return JsonResponse(data)
            else:
                # message
                data = {
                    'message': 'Las contraseñas no coinciden',
                }

                return JsonResponse(data)

        else:
            data = {
                'message': 'El usuario ya existe, comprueba tus datos'
            }

            return JsonResponse(data)
    else:
        form = SignUpFormExtended()
    return render(request, 'SignUp/SignUp.html', {
        'forms': form,
    })

# finish session


def SignOut(request):
    logout(request)
    return redirect('/')


# Manage account
def ManageAccount(request):

    # detect if user is on session
    if request.user.is_authenticated:
        # get current id user
        id = request.user.id
        # qs
        info = UserProfile.objects.select_related('user').filter(user_id=id)

        # return template
        return render(request, 'Account/Manage.html', {
            'inf': info,
        })
    else:
        return redirect('account:sign_in')

# update account


def GetAccountInfo(request, token):
    # check if user is connected
    if request.user.is_authenticated:
        # usando manager = probar si funciona de manera efectiva
        # info = UserProfile.UserProfiled.filter(token_usr=token)
        info = UserProfile.objects.select_related(
            'user').filter(token_usr=token)
        all_dep = CurrentLocationUsers.objects.all()

        print("Mira esto ===>", info)
        # Loop
        for ss in info:
            print("Info ====>", ss)
            forms = UpdateAccountForm(
                initial={
                    'first_name': ss.user.first_name,
                    'last_name': ss.user.last_name,
                    'email': ss.user.email,
                    'username': ss,
                }
            )

            # get id location
            dep = CurrentLocationUsers.objects.filter(
                id=ss.departamento_id,
            )

            # render html view
            return render(request, 'Account/Edit.html', {
                'form': forms,
                'usr_info': info,
                'deps': dep,
                'all_dep': all_dep,
            })
        #
    else:
        return redirect('account:sign_in')


def UpdateAccount(request):

    # check if user are connected
    if request.user.is_authenticated:
        # check method
        if request.method == "POST":
            # receive data sent from js
            fname = request.POST["first_name"]
            lname = request.POST["last_name"]
            usrname = request.POST["username"]
            mail = request.POST["email"]
            location = request.POST["departamento"]
            token = request.POST['token']

            # check if get data
            if fname != '' and lname != '' and usrname != '' and mail != '' and location != '':
                # find user
                usr = UserProfile.UserProfiled.get(token_usr=token)
                # compare usernames and mails
                if usr.user.username == usrname and usr.user.email == mail:
                    # update names
                    upd = User.objects.filter(
                        id=usr.user_id,
                    ).update(
                        first_name=fname,
                        last_name=lname
                    )
                    # update location
                    upd_ext = UserProfile.UserProfiled.filter(
                        token_usr=usr.token_usr,
                    ).update(
                        departamento=location
                    )
                    # check if process was completed
                    if upd or upd_ext:
                        data = {
                            'message': 'Perfil Actualizado'
                        }
                        return JsonResponse(data)
                    else:
                        data = {
                            'error': 'Oops, algo resulto mal'
                        }
                        return JsonResponse(data)
                else:
                    # update all records
                    upd = User.objects.filter(
                        id=usr.user_id
                    ).update(
                        first_name=fname,
                        last_name=lname,
                        username=usrname,
                        email=mail
                    )
                    # update info
                    upd_ext = UserProfile.UserProfiled.filter(
                        token_usr=usr.token_usr,
                    ).update(
                        departamento=location
                    )
                    # Check if process was completed
                    if upd or upd_ext:
                        data = {
                            'message': 'Todos los datos han sido actualizados'
                        }
                        return JsonResponse(data)
                    else:
                        data = {
                            'error': 'Error, intenta de nuevo'
                        }
                        return JsonResponse(data)
            else:
                data = {
                    'error': 'Todos los campos son necesarios'
                }
                return JsonResponse(data)
    else:
        return redirect('account:edit_account')


def Update_Pic(request, token):

    # check method
    if request.method == "POST":

        # values to upload img
        img = request.FILES['img_save']
        fs = FileSystemStorage()
        filename = fs.save(img.name, img)
        img_path = fs.url(filename)

        # get user and try to update profile picture
        upd = UserProfile.UserProfiled.filter(
            token_usr=token,
        ).update(
            p_profile=img_path,
        )

        # Check if process was completed
        if upd:
            return redirect('account:manage')
        else:
            return redirect('account:edit_account')
    else:
        return redirect('account:manage')


@login_required
def UpdatePassword(request):

    # Detect method
    if request.method == 'POST':

        # get value from html
        token = request.POST['token']
        pass1 = request.POST['password_1']
        pass2 = request.POST['password_2']

        if pass1 != '' and pass2 != '':

            # compare pass
            if pass1 == pass2:
                # update method
                print(pass1)
                print(pass2)

                # update password
                usr = UserProfile.UserProfiled.filter(token_usr=token)

                print(usr[0])

                # update method
                upd = User.objects.get(username=usr[0])
                upd.set_password(pass1)
                upd.save()

                if upd:
                    # msg of success
                    data = {
                        'msg': 'Contraseña actualizada'
                    }

                    return JsonResponse(data)
                else:

                    # msg of error
                    data = {
                        'err': 'Error, comprueba tu contraseña'
                    }

                    return JsonResponse(data)
            else:

                # message of error
                data = {
                    'err': 'Las contraseñas no coinciden'
                }

                return JsonResponse(data)
        else:

            data = {
                'error': 'Los campos estan vacios'
            }

            return JsonResponse(data)

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import UserProfile


# Creating forms
class SignInForm(forms.Form):
	
	# creating fields
	username = forms.CharField(
		widget=forms.TextInput(attrs={
			'class':'form-control',
		})
	)
	
	password = forms.CharField(
		widget=forms.PasswordInput(attrs={
			'class':'form-control'
		})
	)

class SignUpFormExtended(UserCreationForm):
	
	# Creating form
	email = forms.EmailField(
		widget=forms.TextInput(
			attrs={
				'class':'form-control',
			}
		), required=True
	)
	
	username = forms.CharField(
		widget=forms.TextInput(
			attrs={
				'class':'form-control',
			}
		), required=True, max_length=10
	)
	
	password1 = forms.CharField(
		widget=forms.PasswordInput(
			attrs={
				'class':'form-control',
			}
		), required=True, min_length=5
	)
	
	password2 = forms.CharField(
		widget=forms.PasswordInput(
			attrs={
				'class': 'form-control',
			}
		), required=True
	)
	
	# Using User model
	class Meta:
		model = User
		fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2')
		
	
	# save method
	def save(self, commit=True):
		user = super().save(commit=False)
		user.username = self.cleaned_data['username']
		user.email = self.cleaned_data['email']
		
		if commit:
			user.save()
		return user
	
class UpdateAccountForm(forms.ModelForm):
	
	# fields
	first_name = forms.CharField(
		widget=forms.TextInput(
			attrs={
				'class':'form-control',
			}
		)
	)
	
	last_name = forms.CharField(
		widget=forms.TextInput(
			attrs={
				'class':'form-control',
			}
		)
	)
	
	email = forms.EmailField(
		widget=forms.TextInput(
			attrs={
				'class':'form-control',
			}
		)
	)
	
	username = forms.CharField(
		widget=forms.TextInput(
			attrs={
				'class':'form-control',
			}
		)
	)
	
	class Meta:
		model = UserProfile
		fields = ('first_name', 'last_name', 'email', 'username')

		
# # Update Password
# class UpdatePasswordForm(forms.ModelForm):
#
# 	# Creating fields
# 	password1 = forms.CharField(
# 		widget=forms.PasswordInput(
# 			attrs={
# 				'class':'form-control',
# 			}
# 		)
# 	)
#
# 	password2 = forms.CharField(
# 		widget=forms.PasswordInput(
# 			attrs={
# 				'class':'form-control',
# 			}
# 		)
# 	)
#
# 	class Meta:
# 		model = UserProfile
# 		fields = ('password1', 'password2')
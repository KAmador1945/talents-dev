// global vars
var fname = document.querySelector("#id_first_name"),
    lname = document.querySelector("#id_last_name"),
    mail = document.querySelector("#id_email"),
    ced = document.querySelector("#id_ced"),
    social_nmbr = document.querySelector("#id_social_nmber"),
    tel = document.querySelector("#id_tel"),
    mobile = document.querySelector("#id_mobile"),
    other = document.querySelector("#id_other"),
    current_l = document.querySelector("#id_departamento"),
    drive = document.querySelector("#id_lic"),
    cat_drive = document.querySelector("#id_cat"),
    // social network
    linkedin = document.querySelector("#id_linkedin"),
    twitter = document.querySelector("#id_twitter"),
    facebook = document.querySelector("#id_facebook"),
    instagram = document.querySelector("#id_instagram"),
    // high school
    high_school = document.querySelector("#id_highSchool_name"),
    high_start_in = document.querySelector("#id_highSchool_start"),
    high_end_in = document.querySelector("#id_highSchool_end"),
    // college
    college_name = document.querySelector("#id_college_name"),
    profession_college = document.querySelector("#id_profession_name"),
    college_start_in = document.querySelector("#id_college_start"),
    college_end_in = document.querySelector("#id_college_end"),
    // masters
    ms_college_name = document.querySelector("#id_ms_college_name"),
    ms_name = document.querySelector("#id_ms_name"),
    ms_start_in = document.querySelector("#id_ms_start"),
    ms_end_in = document.querySelector("#id_ms_end"),
    // doctor
    dt_college_name = document.querySelector("#id_dt_college_name"),
    dt_name = document.querySelector("#id_dt_name"),
    dt_start_in = document.querySelector("#id_dt_start"),
    dt_end_in = document.querySelector("#id_dt_end"),
    // experiences
    company_name = document.querySelector("#id_cp_name"),
    position_name = document.querySelector("#id_cp_position"),
    cp_start_in = document.querySelector("#id_cp_start_in"),
    cp_end_in = document.querySelector("#id_cp_end_in"),
    // skills
    languages = document.querySelector("#id_language"),
    other_skills = document.querySelector("#id_other"),
    courses = document.querySelector("#id_courses_1"),
    other_courses = document.querySelector("#id_other_course"),
    // professional references
    full_name_ref = document.querySelector("#id_full_name_ref"),
    mobile_ref = document.querySelector("#id_mobile_ref"),
    other_ref = document.querySelector("#id_other_ref"),
    email_ref = document.querySelector("#id_email_ref"),
    // about me
    about_me = document.querySelector("#id_bio_me"),
    // csrtoken 
    csrtoken = document.querySelector('input[name="csrfmiddlewaretoken"]');

// Create professional
function CreateProf() {
    //define path
    let url = `/create/create_p/`;

    // Ajax method
    $.ajax({
        url: url,
        method:"POST",
        data: {
            first_name:fname.value,
            last_name:lname.value,
            email:mail.value,
            ced:ced.value,
            social:social_nmbr.value,
            tel:tel.value,
            mobile:mobile.value,
            other:other.value,
            loc:current_l.value,
            drive:drive.value,
            cat_drive:cat_drive.value,
            linkedin:linkedin.value,
            twitter:twitter.value,
            facebook:facebook.value,
            instagram:instagram.value,
            csrfmiddlewaretoken:csrtoken.value,
        }, success: function(message) {
            // detect if exist any error
            if (message.error === "error") {
                console.log(message.error)
            } 
            console.log(message.message);
        }
    });
    

    // // Ajax method
    // $.ajax({
    //     url:url,
    //     method:"POST",
    //     data: {
    //         first_name:fname.value,
    //         last_name:lname.value,
    //         email:mail.value,
    //         csrfmiddlewaretoken: csrtoken.value,
    //     }, success: function(message) {
    //         if (message.error === 'error') {
    //             new Noty({
    //                 type:'error',
    //                 layout: 'topRight',
    //                 text:message.error,
    //                 timeout:2500,
    //                 theme: 'mint',
    //             }).show();
    //         } else {
    //             //show notify
    //             new Noty({
    //                 type: 'success',
    //                 layout: 'topRight',
    //                 text: message.message,
    //                 timeout: 2500,
    //                 theme: 'mint',
    //             }).show();
    //         }
    //     }, error: function(req, err) {
    //         console.log(req, err);
    //     }
    // });

}
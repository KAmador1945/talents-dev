//Define event
const UpdatePass = (token) => {
    // define local vars
    let pass = document.querySelector("#id_password"),
        pass2 = document.querySelector('#id_password2'),
        csrtok = document.querySelector('input[name="csrfmiddlewaretoken"]'),
        url = '/account/manage/edit/up/';

    // Check if exist any data
    if (token !== '' || pass.value !== '' || pass2.value !== '') {
        //ajax method
        $.ajax({
            url:url,
            method:"POST",
            dataType:"JSON",
            data: {
                password_1:pass.value,
                password_2:pass2.value,
                token:token,
                csrfmiddlewaretoken: csrtok.value,
            }, success: function (message) {
                if (message.msg) {
                    new Noty({
                        type:'success',
                        layout: 'topRight',
                        text:message.msg,
                        timeout:2500,
                        theme: 'mint',
                    }).show();

                    //Realod page
                    setTimeout(() => {
                        window.location = '/account/manage/';
                    }, 3500);

                } else {
                    new Noty({
                        type:'error',
                        layout: 'topRight',
                        text:message.err,
                        timeout:2500,
                        theme: 'mint',
                    }).show();
                }
            }, error: function (err , req) {
                console.log(err, req);
            }
        })
    }
};
var username = document.querySelector('#id_username'),
    password = document.querySelector('#id_password'),
    btn = document.querySelector('#signin_btn'),
    csrtoken = document.querySelector('input[name="csrfmiddlewaretoken"]');


//Sign In
function SignIn() {
    //url
    let url = '/account/sign_in/';

    //check if exist data in every field
    if (username.value === '' || password.value === '') {

        //add border
        username.style.border = '2px solid red';
        password.style.border = '2px solid red';

        //Creating notification
        new Noty({
            type:'error',
            layout: 'topRight',
            text:'Error, todos los campos son requeridos.',
            timeout: 2500,
            theme:'mint',
        }).show();

    } else {
        // Ajax method
        $.ajax({
            method:"POST",
            url:url,
            data: {
                username: username.value,
                password: password.value,
                csrfmiddlewaretoken: csrtoken.value,
            },
            dataType:'json',
            success: function (data) {
                if (data.message) {
                    new Noty({
                        type:'alert',
                        layout: 'topRight',
                        text:data.message,
                        timeout: 2500,
                        theme:'mint',
                    }).show();

                    //Send to home
                    setTimeout(function () {
                        window.location.href = '/';
                    }, 3500);
                }
            }, error: function (err, resp) {
                console.log(err, resp);
            }
        });
    }
}
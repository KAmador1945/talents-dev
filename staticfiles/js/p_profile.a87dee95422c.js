var p_pic = document.querySelector('#imgLoad'),
    csrtoken = document.querySelector('input[name="csrfmiddlewaretoken"]');


$(document).ready(function () {
    $("#imgLoad").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#imgShow").attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    })
});

//Load modal
function ShowModal() {
    //show modal
    $("#UpdateProfilePic").modal('show');
}

//Send data
function Update_p(token) {

    console.log(token);

    var data = new FormData($('#update_pic').get(0));

    console.log(data);

    //define url
    let url = '/account/manage/edit/pp/';

    // check if toke has data
    if(token !== '') {
        //ajax method
        $.ajax({
            url:url,
            method:"POST",
            data: {
                img_save:p_pic.value,
                token:token,
                csrfmiddlewaretoken:csrtoken.value,
            },
            success: function (resp) {
                console.log(resp);
            }, error: function (err, req) {
                console.log(err, req);
            }
        })
    }
}
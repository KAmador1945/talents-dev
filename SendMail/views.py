from django.core.mail import send_mail
import threading

# Create your views here.
class EmailThreat(threading.Thread):
	
	# create thread
	def __init__(self, subject, message, from_mail, to):
		
		send = send_mail(
			subject=subject,
			message=message,
			from_email=from_mail,
			recipient_list=to,
			fail_silently=False,
		)
		
		threading.Thread.__init__(self)
		
	# send thread
	def run(self):
		self.send
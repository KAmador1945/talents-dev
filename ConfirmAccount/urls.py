from django.urls import path
from .views import VerifyAccount, UpdateStatusAccount

app_name = 'confirm'

urlpatterns = [
	path('verificate/<str:token>', VerifyAccount, name='verificate'),
	path('confirm/', UpdateStatusAccount, name='confirm'),
]
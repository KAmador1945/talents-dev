from django.shortcuts import render, redirect
from django.http import JsonResponse
from account.models import UserProfile

# verify account
def VerifyAccount(request, token):
	# detect if user is connected
	if request.user.is_authenticated:
		# get status user
		# status = UserProfile.objects.filter(status='INACTIVE').get(token_usr=token)

		# get token from url
		if token is not None:
			# making query
			status = UserProfile.objects.get(token_usr=token)

			# asking by status
			if status.active == False:

				# Returning interface
				return render(request, 'Account/confirm_message.html', {
					'std':status,
				})
			else:
				return redirect('account:manage')
		else:
			return redirect('account:sign_in')


# Update account
def UpdateStatusAccount(request):

	# check method
	if request.method == "POST":
		# get token
		token = request.POST['token']
		# check if exist any data
		if token is not None:
			upd = UserProfile.objects.filter(token_usr=token).update(
				status='ACTIVE',
				active=True,
			)
			# send message after account has been send
			if upd:
				data = {
					'message': 'La cuenta ha sido activada exitosamente',
					'token': token,
				}
				return JsonResponse(data)

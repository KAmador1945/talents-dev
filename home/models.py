from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.
class ProfessionArea(models.Model):

    class Meta:
        verbose_name = "Profession Area"
        verbose_name_plural = "Profession Area"

    # fields
    area_name = models.CharField(max_length=50, blank=False)
    image_icon = models.ImageField(upload_to='resources', null=True, blank=True)

    def __str__(self):
        return self.area_name


class TalentsInfo(models.Model):

    class Meta:
        verbose_name = "Talents Info"
        verbose_name_plural = "Talents Info"

    # fields
    info_data = models.CharField(max_length=50, blank=False)
    count = models.CharField(max_length=50, blank=False)
    img_icon = models.ImageField(upload_to='resources', null=True, blank=True)

    def __str__(self):
        return self.info_data
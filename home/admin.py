from django.contrib import admin
from home.models import ProfessionArea, TalentsInfo


# Settings
class ProfessionAreaAdmin(admin.ModelAdmin):
    # read only
    readonly_fields = (
        'created',
        'updated'
    )

# register model in admin
admin.site.register(ProfessionArea)

class TalentsInfoAdmin(admin.ModelAdmin):
    # read only
    readonly_fields = (
        'created',
        'updated'
    )
admin.site.register(TalentsInfo)

# # settings 
# class ProfessionAdmin(admin.ModelAdmin):
#     # read only
#     readonly_fields = (
#         'created',
#         'updated'
#     )

# # register model in admin
# admin.site.register(Profession)
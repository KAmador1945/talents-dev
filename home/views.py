from django.shortcuts import render
from home.models import ProfessionArea, TalentsInfo

# Create your views here.
# 	Home
def Home(request):

	# qs to Profession
	qs = ProfessionArea.objects.all() #select_related('prof_area')

	# gets talents info
	qs_t = TalentsInfo.objects.all()

	# load home
	return render(request, 'Home/Home.html', {
		'qs':qs,
		'qs_t':qs_t,
	})

# qs
# user = UserProfile.objects.all()
#
# # if detect session
# if request.user.is_authenticated:
# 	# returning template
# 	return render(request, 'Home/Home.html', {
# 		'user':user,
# 	})
#
# if not request.user.is_authenticated:
# 	return redirect('account:sign_in')

csrtoken = document.querySelector('input[name="csrfmiddlewaretoken"]'),
    btn = document.querySelector('#confirm_account');

// confirm function
function ConfirmAccountUser(token) {

    //define url
    let url = '/confirm/confirm/';

    //check if exist any data
    if (token != '' && csrtoken.value != '') {

        // disabling btn
        btn.disabled = true;

        // Ajax method
        $.ajax({
            method:'POST',
            url:url,
            data: {
                token: token,
                csrfmiddlewaretoken: csrtoken.value,
            },
            success: function (resp) {
                if (resp != '') {
                    //Send notification
                         new Noty({
                            type:'success',
                            layout: 'topRight',
                            text: resp.message,
                            timeout: 2500,
                            theme:'mint',
                        }).show();

                         console.log(resp.message);

                        //Send to home after to register account
                        setTimeout(function () {
                            window.location.href = `/account/manage/edit/${resp.token}`;
                        }, 3000);
                }
            },
            error: function (req, err) {
                console.log(req, err);
            }
        })
    }
}
// global vars
var current_addrss = document.querySelector("#id_address"),
    card_id = document.querySelector("#id_ced"),
    tel = document.querySelector("#id_tel"),
    mobile = document.querySelector("#id_mobile"),
    other = document.querySelector("#id_other"),
    drive = document.querySelector("#id_lic"),
    cat_drive = document.querySelector("#id_cat"),
    // social network
    linkedin = document.querySelector("#id_linkedin"),
    twitter = document.querySelector("#id_twitter"),
    facebook = document.querySelector("#id_facebook"),
    instagram = document.querySelector("#id_instagram"),
    // college
    college_name = document.querySelector("#id_college_name"),
    profession_college = document.querySelector("#id_profession_name"),
    college_start_in = document.querySelector("#id_college_start"),
    college_end_in = document.querySelector("#id_college_end"),
    // masters
    ms_college_name = document.querySelector("#id_ms_college_name"),
    ms_name = document.querySelector("#id_ms_name"),
    ms_start_in = document.querySelector("#id_ms_start"),
    ms_end_in = document.querySelector("#id_ms_end"),
    // doctor
    dt_college_name = document.querySelector("#id_dt_college_name"),
    dt_name = document.querySelector("#id_dt_name"),
    dt_start_in = document.querySelector("#id_dt_start"),
    dt_end_in = document.querySelector("#id_dt_end"),
    // experiences
    company_name = document.querySelector("#id_cp_name"),
    position_name = document.querySelector("#id_cp_position"),
    cp_start_in = document.querySelector("#id_cp_start_in"),
    cp_end_in = document.querySelector("#id_cp_end_in"),
    // professional references
    prof_full_name_ref = document.querySelector("#id_ref_name"),
    prof_mobile_ref = document.querySelector("#id_ref_tel"),
    prof_other_ref = document.querySelector("#id_other_ref"),
    prof_email_ref = document.querySelector("#id_ref_email"),
    // personal references
    pers_refe_name = document.getElementById("id_full_name_per_ref"),
    pers_refe_numb = document.getElementById("id_mobile_per_ref"),
    pers_refe_other_numb = document.getElementById("id_other_per_ref"),
    pers_refe_email = document.getElementById("id_email_per_ref"),
    // skills
    languages = document.querySelector("#id_language"),
    other_skills = document.querySelector("#id_courses_skills"),
    courses_skills = document.querySelector("#courses_skills"),
    other_courses = document.querySelector("#id_other_course"),
    // about me
    about_me = document.querySelector("#id_bio_me"),
    // csrtoken 
    csrtoken = document.querySelector('input[name="csrfmiddlewaretoken"]'),
    // get button
    btns = document.querySelector("#siguiente"),
    btns_sn = document.querySelector("#siguiente_sn"),
    btns_study = document.querySelector("#siguiente_study"),
    btns_prof_ref = document.querySelector("#prof_references"),
    prof_exp_btn = document.querySelector("#profe_exp"),
    personal_ref_btn = document.querySelector("#personal_ref_btn"),
    skills_btns = document.querySelector("#skills"),
    complete_resume = document.querySelector("#complete"),
    // tokens
    token;
// auto execute
(() => {
    DisabledBtns();
})();
// Create professional
function CreateProf(tok) {
    //define path
    let url = '/create/professional_profile/edit/';
    token = tok;
    // detect if exist data in every var
    if(current_addrss.value !== "" || card_id.value !== "" || mobile.value !== "" ||  cat_drive.value !== "") {
        $.ajax({
            url:url,
            method:"POST",
            data: {
                'token_usr':token,
                'ced':card_id.value,
                'current_address':current_addrss.value,
                'tel_number':tel.value,
                'mobile_number':mobile.value,
                'other_number':other.value,
                'lic_permission':drive.value,
                'drive_lic_cat':cat_drive.value,
                csrfmiddlewaretoken: csrtoken.value,
            }, success: function(message) {
                console.log(message);
            }, error: function(err, req) {
                console.log(err, req);
            }
        });
    } else {
        alert("Error")
    }
}
// Social Media
function SocialLinks(tok) {
    let url = '/create/professional_profile/edit/sm/';
    token = tok;
    // detect if exist data
    if (linkedin.value !== "" || twitter.values !== "" || facebook.value !== "" || instagram.value !== "") {
        $.ajax({
            url:url,
            method:"POST",
            data: {
                'token_usr':token,
                'linkedin':linkedin.value,
                'twitter':twitter.value,
                'facebook':facebook.value,
                'instagram':instagram.value,
                csrfmiddlewaretoken: csrtoken.value,
            },success: function(message) {
                if (message.message !== "") {
                    console.log(message.message);
                }
                if (message.error) {
                    console.log(message.error);
                }
            }, error: function(req, err) {
                console.log(req, err);
            }
        })
    } else {
        alert("Error");
    }
}
// Studies
function Studies(tok) {
    let url = '/create/professional_profile/edit/std/';
    token = tok;
    // detect if exist data
    if (college_name.value !== "" || profession_college.value !== "" || college_start_in.value !== "" || college_end_in.value ==! "") {
        $.ajax({
            url:url,
            method:'POST',
            data:{
                'token_usr':token,
                'college_name':college_name.value,
                'profession_college':profession_college.value,
                'college_start':college_start_in.value,
                'college_end':college_end_in.value,
                csrfmiddlewaretoken: csrtoken.value,
            },success: function(message) {
                if(message.message !== "") {
                    console.log(message.message);
                }
                if (message.error) {
                    console.log(message.error);
                }
            }, error: function(req, err) {
                console.log(req, err);
            }
        });
    } else {
        alert("Error");
    }
}
// Experiences
function Experiences(tok) {
    let url = '/create/professional_profile/edit/pe/';
    token = tok;
    // detect if exist data
    if (company_name.value !== "" || position_name.value !== "" || cp_start_in.value !== "" || cp_end_in.value !== "") {
        $.ajax({
            url:url,
            method:'POST',
            data:{
                'token_usr':token,
                'company_name':company_name.value,
                'position_name':position_name.value,
                'cp_start':cp_start_in.value,
                'cp_end':cp_end_in.value,
                csrfmiddlewaretoken: csrtoken.value,
            },success: function(message) {
                if (message !== "") {
                    console.log(message.message);
                }
                if (message.error) {
                    console.log(message.error);
                }
            }, error: function(req, err) {
                console.log(req, err);
            }
        });
    } else {
        alert("Error");
    }
}
// Create personal references
function ProfessionalReferences(tok) {
    let url = '/create/professional_profile/edit/pr/';
    token = tok;
    // detect if exist data
    if(prof_full_name_ref.value !== "" || prof_mobile_ref.value !== "" || prof_other_ref.value !== "" || prof_email_ref.value !== "") {
        $.ajax({
            url:url,
            method:'POST',
            data:{
                'token_usr':token,
                'full_name_ref':prof_full_name_ref.value,
                'mobile':prof_mobile_ref.value,
                'other':prof_other_ref.value,
                'email':prof_email_ref.value,
                csrfmiddlewaretoken: csrtoken.value,
            },
            success: function(message) {
                if(message !== "") {
                    console.log(message.message)
                }
            }, error: function(req, err) {
                console.log(req,err);
            }
        })
    } else {
        // TODO, add a flag to error alert and show it to the user
        console.log("Oops, all field are requiered")
    }
}
// create personal references
function PersonalReferences(tok) {
    let url = '/create/professional_profile/edit/per_ref/';
    token = tok;

    // detect if exist data
    if(pers_refe_name.value !== "" || pers_refe_numb.value !== "" || pers_refe_other_numb.value !== "" || pers_refe_email.value !== "") {
        // Ajax method
        $.ajax({
            url:url,
            method:'POST',
            data:{
                'token_usr':token,
                'name_per_ref':pers_refe_name.value,
                'mobile_per_ref':pers_refe_numb.value,
                'other_numb_per_ref':pers_refe_other_numb.value,
                'email_ref_per':pers_refe_email.value,
                csrfmiddlewaretoken: csrtoken.value,
            },
            success: function(message) {
                console.log(message)
            }, error: function(err, req) {
                console.log(err, req)
            }
        });
    }
}
// Create skills
function CreateSkills(tok) {
    let url = '/create/professional_profile/edit/skills/';
    token = tok;
    // detect if exist data
    if (languages.value !== "" || other_skills.value !== "" || courses_skills.value !== "" || other_courses.value !== "") {
        console.log(languages.value);
        console.log(other_skills.value);
        console.log(courses_skills.value);
        console.log(other_courses.value);
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                'usr_token':token,
                'language':languages.value,
                'other_skills':other_skills.value,
                'courses_study':courses_skills.value,
                'other_courses':other_courses.value,
                csrfmiddlewaretoken: csrtoken.value,
            },
            success: function(message) {
                console.log(message);
            }, error: function(err, req) {
                console.log(err, req);
            }
        })
    }

}

// add event and try to enable btn
document.addEventListener('keypress', () => {
    EnableBnts(btns);
});

linkedin.addEventListener('keypress', () => {
    EnableBnts(btns_sn);
});

college_name.addEventListener('keypress', () => {
    EnableBnts(btns_study);
});

company_name.addEventListener('keypress', () => {
    EnableBnts(prof_exp_btn);
});

prof_full_name_ref.addEventListener('keypress', () => {
    EnableBnts(btns_prof_ref);
});

pers_refe_name.addEventListener('keypress', () => {
    EnableBnts(personal_ref_btn);
});
languages.addEventListener('keypress', () => {
    EnableBnts(skills_btns);
})

// Disabled buttons
function DisabledBtns() {
    CheckingBtns(btns);
    CheckingBtns(btns_sn);
    CheckingBtns(btns_study);
    CheckingBtns(btns_prof_ref);
    CheckingBtns(prof_exp_btn);
    CheckingBtns(personal_ref_btn);
    CheckingBtns(skills_btns);
}
// disabled 
function CheckingBtns(btns) {
    // checking value 
    if (card_id.value !== "" || linkedin.value !== "" || profession_college.value !== "" || company_name.value !== "" || prof_full_name_ref.value !== "" || per_ref_full_name.value !== "" || languages.value !== "") {
        btns.disabled = true;
    }
}
// Enable
function EnableBnts(btns) {
    // disable btns
    let title = "Actualizar";
    btns.disabled=false;
    btns.innerText=title;
}
//local vars
var username = document.querySelector('#id_username'),
    email = document.querySelector('#id_email'),
    password1 = document.querySelector('#id_password1'),
    password2 = document.querySelector('#id_password2'),
    csrtoken = document.querySelector('input[name="csrfmiddlewaretoken"]');


//Create account
function SignUp() {

    //url
    let url = '/account/sign_up/';

    //check if every field get data
    if (username.value === '' ||  email.value === '' || password1.value === '' || password2.value === '') {

        //pass style to fields empty
        username.style.borderBottom = '2px solid red';
        email.style.borderBottom = '2px solid red';
        password1.style.borderBottom = '2px solid red';
        password2.style.borderBottom = '2px solid red';

        //pass notification
        new Noty({
            type:'error',
            layout: 'topRight',
            text:'Error, todos los campos son requeridos.',
            timeout: 2500,
            theme:'mint',
        }).show();
    } else {
        //compare two password, if them are same or return true the account will create.
        if (password1.value === password2.value) {
            //disable button
            let btn = document.querySelector("#sign_btn");
            btn.disabled = true;
            //Ajax method
            $.ajax({
                method:'POST',
                url: url,
                data: {
                    username: username.value,
                    email: email.value,
                    password1:password1.value,
                    password2:password2.value,
                    csrfmiddlewaretoken: csrtoken.value,
                }, success: function (message) {
                    if (message != '') {
                        //Send notification
                         new Noty({
                            type:'success',
                            layout: 'topRight',
                            text: message.message,
                            timeout: 2500,
                            theme:'mint',
                        }).show();

                        //Send to home after to register account
                        setTimeout(function () {
                            window.location.href = `/confirm/verificate/${message.usr}`;
                        }, 3000);

                    }
                }, error: function (err, req) {
                    console.log(err, req);
                }
            })
        }
    }

}

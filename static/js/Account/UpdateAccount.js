//define url
var fname = document.querySelector('#id_first_name'),
   lname = document.querySelector('#id_last_name'),
   username = document.querySelector('#id_username'),
   mail = document.querySelector('#id_email'),
  current_l = document.querySelector('#id_departamento'),
  csrtoken = document.querySelector('input[name="csrfmiddlewaretoken"]');


// Update Account
function UpdateAccount(token) {

  // define path
  let url = `/account/manage/edit/ua/`;

    //Ajax method
    $.ajax({
      url: url,
      method:"POST",
      data: {
        first_name:fname.value,
        token: token,
        last_name:lname.value,
        username:username.value,
        email:mail.value,
        departamento:current_l.value,
        csrfmiddlewaretoken:csrtoken.value,
      }, success: function (message) {
        //detect if exist any error
        if (message.error === 'error') {
          //show notify
          new Noty({
            type:'error',
            layout: 'topRight',
            text:message.error,
            timeout:2500,
            theme: 'mint',
          }).show();
        }

        if (message.message !== '') {
          //show notify
          new Noty({
            type: 'success',
            layout: 'topRight',
            text: message.message,
            timeout: 2500,
            theme: 'mint',
          }).show();

          //returning to manage
          setTimeout(() => {
            window.location.href = '/account/manage/';
          }, 3500);
        }
      }, error: function (req, err) {
        console.log(req, err);
      }
    });
}

